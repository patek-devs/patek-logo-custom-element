# Pátek Logo Custom Element

Reusable custom element for the Pátek logo.

## Installation
Include the patek-logo.js file into your HTML file.
```html
<script src="path/to/patek-logo.js"></script>
```

## Usage
```html
<patek-logo title="<your title here>" subtitle="<your subtitle here>">Fallback value here</patek-logo>
```
If no `title` or `subtitle` attribute is set, it defaults to `"Pátek"` and `""` respectively.

The fallback value is used only in browsers without support for custom elements
or with JavaScript disabled.

The logo **will** obey all CSS rules.
## Examples
You can find examples in [examples.html](examples.html).
